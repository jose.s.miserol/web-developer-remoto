<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::view('/','landing.templates.login')->name('landing.login');

Route::prefix('dashboard')->name('dashboard')->group(function () {

	Route::get('/principal', 'DashboardController@index')->name('.welcome');
	Route::get('/usuarios', 'UserController@index')->name('.users');
	Route::prefix('usuario')->name('.user')->group(function () {
	    Route::get('/crear', 'UserController@create')->name('.create');
		Route::post('/crear', 'UserController@store')->name('.store');
		Route::get('/{user}', 'UserController@edit')->name('.profile');
		Route::put('/{user}', 'UserController@update')->name('.update');
		Route::delete('/{user}', 'UserController@destroy')->name('.destroy');
	});

});

