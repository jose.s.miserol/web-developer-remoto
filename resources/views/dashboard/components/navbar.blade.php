<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <a class="navbar-brand" href="#">Dashboard</a>
    </div>
    <div class="collapse navbar-collapse justify-content-end">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="#">
            <i class="material-icons">dashboard</i>
            <p class="d-lg-none d-md-block">
              Dashboard
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">
            <i class="material-icons">person</i>
            <p class="d-lg-none d-md-block">
              Account
            </p>
          </a>
        </li>
      </ul>
    </div>
  </div>
</nav>