<footer class="footer">
  <div class="container-fluid">
    <nav class="float-left">
      <ul>
        <li>Web Developer Remoto</li>
      </ul>
    </nav>
    <div class="copyright float-right">
      &copy; <script> document.write(new Date().getFullYear()) </script> Sebastian Miserol .
    </div>
  </div>
</footer>