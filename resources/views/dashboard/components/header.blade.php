<div class="sidebar" data-color="purple" data-background-color="white" data-image="dashboard/assets/img/sidebar-1.jpg">
  <div class="logo">
    <a href="#" class="simple-text logo-normal">
      Web Developer R.
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
    <li class="nav-item active">
        <a class="nav-link" href="{{ route('dashboard.welcome') }}">
          <i class="material-icons">dashboard</i>
          <p>Dashboard</p>
        </a>
    </li>
    <!--<li class="nav-item ">
        <a class="nav-link" href="#">
          <i class="material-icons">notifications</i>
          <p>Notifications</p>
        </a>
    </li>-->
    <li class="nav-item ">
        <a class="nav-link" href="{{ route('dashboard.users') }}">
          <i class="material-icons">person</i>
          <p>Usuarios</p>
        </a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="{{ route('dashboard.user.create') }}">
          <i class="material-icons">person</i>
          <p>Crear Usuario</p>
        </a>
    </li>
    </ul>
  </div>
</div>