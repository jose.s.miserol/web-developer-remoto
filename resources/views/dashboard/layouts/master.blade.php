<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" prefix="og: http://ogp.me/ns#">

<head>
    <title>@yield('title') - Web Developer R.</title>
    @include('dashboard.components.head')
    @include('dashboard.components.style')
</head>

<body>
    <!-- include('dashboard.components.header')-->
    @yield('content')
    <!-- include('dashboard.components.footer')-->
    @include('dashboard.components.script')
</body>

</html>