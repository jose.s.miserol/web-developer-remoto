@extends('dashboard.layouts.master')

@section('content')

  <div class="wrapper ">
    
    @include('dashboard.components.header')

    <div class="main-panel">

      @include('dashboard.components.navbar')

      @include('dashboard.components.errors', ['error' => $errors])

      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Editar Usuario</h4>
                  <p class="card-category">Información a registrar:</p>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{ route('dashboard.user.update' , $user->id) }}">

                    @csrf

                    {{ method_field('PUT') }}

                    <div class="row">                      
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nombre</label>
                          <input id="name" name="name" type="text" class="form-control" value="{{ $user->name }}">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Email</label>
                          <input id="email" name="email" type="email" class="form-control" value="{{ $user->email }}">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Password</label>
                          <input id="password" name="password" type="password" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Password</label>
                          <input id="password_confirmation" name="password_confirmation" type="password" class="form-control">
                        </div>
                      </div>
                    </div>
                      
                    <button type="submit" class="btn btn-primary pull-right">Editar usuario</button>
                    
                    <div class="clearfix"></div>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      @include('dashboard.components.footer')

    </div>
  </div>

@endsection
