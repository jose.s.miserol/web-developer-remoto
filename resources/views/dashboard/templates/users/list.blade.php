@extends('dashboard.layouts.master')

@section('content')

  <div class="wrapper ">
    
    @include('dashboard.components.header')

    <div class="main-panel">
      
      @include('dashboard.components.navbar')

      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title"> Lista de Usuarios </h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">


                    
                    <table class="table">
                      <thead class=" text-primary">
                        <th> ID </th>
                        <th> Nombre </th>
                        <th> Email </th>
                        <th> Creado </th>
                        <th> Editado </th>
                        <th> Acciones </th>
                      </thead>
                      <tbody>
                        @foreach ($users as $user)
                          <tr>
                            <td>
                              <a href="{{ route('dashboard.user.profile', $user->id) }}">
                                {{ $user->id }}
                              </a>
                            </td>
                            <td>
                              <a href="{{ route('dashboard.user.profile', $user->id) }}">
                                {{ $user->name }}
                              </a>
                            </td>
                            <td>
                              <a href="{{ route('dashboard.user.profile', $user->id) }}">
                                {{ $user->email }}
                              </a>
                            </td>
                            <td>{{ $user->created_at }}</td>
                            <td>{{ $user->update_at }}</td>
                            <td class="text-primary">
                              <form method="POST" action="{{ route('dashboard.user.destroy', $user->id) }}">
                                @csrf

                                {{ method_field('DELETE') }}
                                
                                <button type="submit" class="btn btn-danger">
                                    Eliminar
                                </button>
                              </form>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>

                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

      @include('dashboard.components.footer')

    </div>
  </div>

@endsection
