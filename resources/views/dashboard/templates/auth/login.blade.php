@extends('dashboard.layouts.master')

@section('content')

    <div class="page-header header-filter" style="background-image: url('../assets/img/bg7.jpg'); background-size: cover; background-position: top center;">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 col-md-6 ml-auto mr-auto">
              <div class="card card-login">
                <form class="form-horizontal form-material" method="POST" action="{{ route('login') }}">

                    @csrf

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif

                  <div class="card-header card-header-primary text-center">
                    <h4 class="card-title">Login</h4>
                  </div>
                  <p class="description text-center"></p>
                  <div class="card-body">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">mail</i>
                        </span>
                      </div>
                      <input id="email" name="email" type="email" class="form-control" placeholder="Email">
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">lock_outline</i>
                        </span>
                      </div>
                      <input id="password" name="password" type="password" class="form-control" placeholder="Password">
                    </div>
                  </div>
                  <div class="footer text-center">
                    <a href="#" class="btn btn-primary btn-link btn-wd btn-lg"></a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
    </div>

@endsection
