@extends('dashboard.layouts.master')

@section('content')

  <div class="wrapper ">
    
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="dashboard/assets/img/sidebar-1.jpg">
      <div class="logo">
        <a href="#" class="simple-text logo-normal">
          Web Developer Remoto
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
        <li class="nav-item active  ">
            <a class="nav-link" href="./dashboard.html">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
        </li>
        <<li class="nav-item ">
            <a class="nav-link" href="./notifications.html">
              <i class="material-icons">notifications</i>
              <p>Notifications</p>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="./user.html">
              <i class="material-icons">person</i>
              <p>Usuarios</p>
            </a>
        </li>
        </ul>
      </div>
    </div>

    <div class="main-panel">
      
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Dashboard</a>
          </div>
          <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#pablo">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Dashboard
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#pablo">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title"> Lista de Usuarios </h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    
                    <table class="table">
                      <thead class=" text-primary">
                        <th> ID </th>
                        <th> Nombre </th>
                        <th> Email </th>
                        <th> Creado </th>
                        <th> Editado </th>
                      </thead>
                      <tbody>
                        <tr>
                          <td> 1 </td>
                          <td> Dakota Rice </td>
                          <td> Niger </td>
                          <td> Oud-Turnhout </td>
                          <td class="text-primary"> $36,738 </td>
                        </tr>
                      </tbody>

                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

      <footer class="footer">
        <div class="container-fluid">
          <nav class="float-left">
            <ul>
              <li>Web Developer Remoto</li>
            </ul>
          </nav>
          <div class="copyright float-right">
            &copy; <script> document.write(new Date().getFullYear()) </script> Sebastian Miserol .
          </div>
        </div>
      </footer>

    </div>
  </div>

@endsection
