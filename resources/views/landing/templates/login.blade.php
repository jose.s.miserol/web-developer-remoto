@extends('landing.layouts.master')

@section('content')

  <div class="page-header header-filter" style="background-image: url('landing/img/bg7.jpg'); background-size: cover; background-position: top center;">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-6 ml-auto mr-auto">
            <div class="card card-login">
              <form class="form-horizontal form-material" method="POST" action="{{ route('login') }}">

                @csrf

                @include('landing.components.errors', ['error' => $errors])

                <div class="card-header card-header-primary text-center">
                  <h4 class="card-title">Iniciar sesión</h4>
                </div>
                <p class="description text-center"></p>
                <div class="card-body">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">mail</i>
                      </span>
                    </div>
                    <input id="email" name="email" type="email" class="form-control" placeholder="Email">
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">lock_outline</i>
                      </span>
                    </div>
                    <input id="password" name="password" type="password" class="form-control" placeholder="Password">
                  </div>
                </div>
                <div class="footer text-center">
                  <input type="submit" class="btn btn-primary btn-link btn-wd btn-lg" value="Iniciar sesión">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  </div>

@endsection
