<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" prefix="og: http://ogp.me/ns#">

<head>
    <title>@yield('title') - Web Developer R.</title>
    @include('landing.components.head')
    @include('landing.components.style')
</head>

<body>
    <!-- include('landing.components.header')-->
    @yield('content')
    <!-- include('landing.components.footer')-->
    @include('landing.components.script')
</body>

</html>